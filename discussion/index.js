console.log("Hello, World!");

// Arrray Methods - JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// [SECTION] Mutator Methods
/*
	- Mutator methods are functions that "mutate" or change an array after they're created.
	- These methods manipulate the original array performing various tasks such as adding and removing elements.
*/

// push()
/*
	- Adds an element in the end of an array and returns the array's length.

	Syntax:
		arrayName.push(element);
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

console.log("Current Array: ")
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log("Mutated Array from push() method: ");
console.log(fruits);

// Adding multiple elements to an array

fruits.push('Avocado', 'Guava');
console.log('Another mutated array from push() method: ');
console.log(fruits);

// pop()
/*
	- Removes the last element in an array and returns the removed element.

	Syntax:
		arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop() method: ');
console.log(fruits);

// unshift()
/*
	- Adds one or more elements at the beginning of an array.

	Syntax:
		arrayName.unshift(elementA, elementB, etc)
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift() method: ")
console.log(fruits);

// shift()
/*
	- Removes an element aat the beginning of an array and returns the removed element.
	
	Syntax:
		arrayName.shift()
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated Array from shift() method: ");
console.log(fruits);

// splice()
/*
	- Simultaneously removes elements from a specified index number and adds another/other elements in its/their place
	
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Lime", "Cherry", "Lemon");
console.log("Mutated Array from splice method");
console.log(fruits);

// Inserting at an index number using splice

fruits.splice(1, 0, "Jackfruit");
console.log(fruits);


// sort()
/*
	- Rearranges the array elements in alphanumeric order.
	
	Syntax:
		arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort() method: ")
console.log(fruits);

const randomThings = ["cat", "boy", "apps", "zoo"]
console.log(randomThings.sort());

// reverse()
/*
	- Reverse the order of array elements

	Syntax:
		arrayName.reverse();
*/

console.log(randomThings.reverse());


// [SECTION] Non-mutator Methods
/*
	- Non-mutator methods are functions that do not modify or change an array after they're created.
	- These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing the output.
*/

// indexOf();
/*
	- Returns the index number of the first matching element found in an array.
	- If no match was found, the result will be -1.
	- The search process will be done from first element proceeding to the last element.

	Syntax:
		arrayName.indexOf(searchValue);
*/
let countries = ["US", "PH", "CA", "SG", "TH", "BR", "FR", "DE"]
let firstIndex = countries.indexOf("CA");
console.log("Result of indexOf() method: ")
console.log(firstIndex);

// lastIndexOf()
/*
	- Returns the last index of the first match
*/

// slice()
/*
	- Portion/slices elements from an array and returns a new array.

	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex); 
*/

// Slicing off elements from a specified index from the first element

let slicedArrayA = countries.slice(4);
console.log("Results from slice() method: ");
console.log(slicedArrayA);
console.log(countries);

// Slicing off elements from a specified index to another index
// Note: the ending element indexed is not included.

let slicedArrayB = countries.slice(0,3);
console.log("Results from slice() method: ")
console.log(slicedArrayB);

// Slicing off elements from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log("Results from slice() method: ");
console.log(slicedArrayC);

// forEach();
/*
	- Similar to a loop that iterates on each array element.
	- For each item in the arrya, the anonymous function passed in forEach() method will be run.
	- arrayName - plural for good practice
	- parameter - singular
	- Note: it must have function

	Syntax: 
		arrayName.forEach(function(indivElement) {
			statement;
		})

} 
*/

countries.forEach(function(country) {
	console.log(country);
})

// includes()
/*
	- includes() method checks if the argument passed can be foundin the array.
	- it returns a boolean which can be saved in a variable.
		-return true if the argument is found in the array.
		-return false if it is not.

	Syntax:
		arrayName.includes(<argumentToFind>) 
*/
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes("Mouse");
console.log(productFound1);//returns true
let productFound2 = products.includes("Headset");
console.log(productFound2);//returns false

